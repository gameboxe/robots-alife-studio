﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

namespace GameBoxe
{
    class DBConnect
    {


        private MySqlConnection connection;
        private string server;
        private string database;
        private string uid;
        private string password;
        private IniParser ini;

        public DBConnect()
        {
            Initialize();
        }

        private void Initialize()
        {
            ini      = new IniParser(Environment.CurrentDirectory + "/settings.ini");
            server   = ini.IniReadValue("MYSQL", "Server");
            database = ini.IniReadValue("MYSQL", "Database");
            uid      = ini.IniReadValue("MYSQL", "Uid");
            password = ini.IniReadValue("MYSQL", "Password");
            string connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" + database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";
            connection = new MySqlConnection(connectionString);
        }

        private bool OpenConnection()
        {
            try
            {
                connection.Open();
                return true;
            }
            catch (MySqlException ex)
            {
                switch (ex.Number)
                {
                    case 0:
                        Console.WriteLine("Cannot connect to server.  Contact administrator");
                        break;

                    case 1045:
                        Console.WriteLine("Invalid username/password, please try again");
                        break;
                }
                return false;
            }
        }

        private bool CloseConnection()
        {
            try
            {
                connection.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public List<string>[] Select()
        {
            ini = new IniParser(Environment.CurrentDirectory + "/settings.ini");
            List<string>[] list = new List<string>[6];
            list[0] = new List<string>();
            list[1] = new List<string>();
            list[2] = new List<string>();
            list[3] = new List<string>();
            list[4] = new List<string>();
            list[5] = new List<string>();

            if (OpenConnection() == true)
            {
                MySqlCommand cmd           = new MySqlCommand("SELECT * FROM " + ini.IniReadValue("SERVER", "Table") + " WHERE serv='" + ini.IniReadValue("SERVER", "Serv") + "'", connection);
                MySqlDataReader dataReader = cmd.ExecuteReader();

                while (dataReader.Read())
                {
                    list[0].Add(dataReader["id"] + "");
                    list[1].Add(dataReader["type"] + "");
                    list[2].Add(dataReader["status"] + "");
                    list[3].Add(dataReader["serv"] + "");
                    list[4].Add(dataReader["uid"] + "");
                    list[5].Add(dataReader["game"] + "");
                }

                dataReader.Close();
                CloseConnection();
                return list;
            }
            else
            {
                return list;
            }
        }

        public int CountWait()
        {
            ini = new IniParser(Environment.CurrentDirectory + "/settings.ini");
            string query = "SELECT Count(*) FROM " + ini.IniReadValue("SERVER", "Table") + " WHERE status='wait' AND serv='" + ini.IniReadValue("SERVER", "Serv") + "'";
            int Count = -1;
            if (OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                Count = int.Parse(cmd.ExecuteScalar() + "");
                CloseConnection();
                return Count;
            }
            else
            {
                return Count;
            }
        }

        public int CountError()
        {
            ini = new IniParser(Environment.CurrentDirectory + "/settings.ini");
            string query = "SELECT Count(*) FROM " + ini.IniReadValue("SERVER", "Table") + " WHERE status='error' AND serv='" + ini.IniReadValue("SERVER", "Serv") + "'";
            int Count = -1;
            if (OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                Count = int.Parse(cmd.ExecuteScalar() + "");
                CloseConnection();
                return Count;
            }
            else
            {
                return Count;
            }
        }

        public int CountAttente()
        {
            ini = new IniParser(Environment.CurrentDirectory + "/settings.ini");
            string query = "SELECT Count(*) FROM " + ini.IniReadValue("SERVER", "Table") + " WHERE status='attente' AND serv='" + ini.IniReadValue("SERVER", "Serv") + "'";
            int Count = -1;
            if (OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                Count = int.Parse(cmd.ExecuteScalar() + "");
                CloseConnection();
                return Count;
            }
            else
            {
                return Count;
            }
        }

        public void Update(string id, string param)
        {
            string query = "UPDATE " + ini.IniReadValue("SERVER", "Table") + " SET status='" + param + "' WHERE id='" + id + "'";

            if (OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandText  = query;
                cmd.Connection   = connection;
                cmd.ExecuteNonQuery();
                CloseConnection();
            }
        }

    }
}
