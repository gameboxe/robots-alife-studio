﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Timers;

namespace GameBoxe
{
    class Program
    {
        private static DBConnect dbConnect;
        private static Timer myTimer = new Timer();

        static void Main(string[] args)
        {

            
            Console.Title = "GameBoxe";

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow   = false;
            startInfo.UseShellExecute  = false;
            startInfo.FileName         = "SteamCMD/steamcmd.exe";
            startInfo.WindowStyle      = ProcessWindowStyle.Hidden;
            startInfo.Arguments        = "+quit";

            try
            {
                Console.Title = "GameBoxe - SteamCMD Installation en cours...";
                using (Process exeProcess = Process.Start(startInfo))
                {
                    exeProcess.WaitForExit();
                }
            } finally {
                Console.Title = "GameBoxe - Attente du Robot...";
                Console.WriteLine("=======================================================================================================================");
                myTimer.Elapsed += OnTimerElapsed;
                myTimer.Interval = 10000;
                myTimer.Start();
            }

            dbConnect = new DBConnect();

            //ConsoleKeyInfo cki;
            while (true)
            {
                //cki = Console.ReadKey(true);
                //Console.WriteLine("  Key pressed: {0}\n", cki.Key);
                //if (cki.Key == ConsoleKey.X) break;
            }
        }

        private static void OnTimerElapsed(object sender, ElapsedEventArgs e)
        {
            int CountWait    = dbConnect.CountWait();
            int CountAttente = dbConnect.CountAttente();
            int CountError   = dbConnect.CountError();

            Console.Title = "GameBoxe - (" + CountWait + " Wait) - (" + CountAttente + " Attente) - (" + CountError + " Error)";
            if (CountWait != 0)
            {
                List<string>[] list;
                list = dbConnect.Select();
                for (int i = 0; i < list[0].Count; i++)
                {

                    var id     = list[0][i];
                    var type   = list[1][i];
                    var status = list[2][i];
                    var uid    = list[4][i];
                    var game   = list[5][i];

                    if (status == "wait")
                    {

                        // HURTWORLD
                        if (game == "hurtworld")
                        {
                            Console.WriteLine("Debug : " + id + " - " + type + " - " + status);
                            ProcessStartInfo startInfo = new ProcessStartInfo();
                            startInfo.CreateNoWindow   = true;
                            startInfo.UseShellExecute  = true;
                            startInfo.FileName         = Environment.CurrentDirectory + "/Hurtworld.exe";
                            startInfo.WindowStyle      = ProcessWindowStyle.Hidden;
                            startInfo.Arguments        = id + " " + type + " " + uid + " 85746";
                            Process.Start(startInfo);
                        }

                        // ARMA3
                        if (game == "arma3")
                        {
                            Console.WriteLine("Debug : " + id + " - " + type + " - " + status);
                            ProcessStartInfo startInfo = new ProcessStartInfo();
                            startInfo.CreateNoWindow   = true;
                            startInfo.UseShellExecute  = true;
                            startInfo.FileName         = Environment.CurrentDirectory + "/Arma3.exe";
                            startInfo.WindowStyle      = ProcessWindowStyle.Hidden;
                            startInfo.Arguments        = id + " " + type + " " + uid + " 85746";
                            Process.Start(startInfo);
                        }

                    }
                }
            }

        }
    }
}
