﻿using MySql.Data.MySqlClient;
using System;

namespace Hurtworld
{
    class DBConnect
    {


        private MySqlConnection connection;
        private string server;
        private string database;
        private string uid;
        private string password;
        private IniParser ini;

        public DBConnect()
        {
            Initialize();
        }

        private void Initialize()
        {
            ini      = new IniParser(Environment.CurrentDirectory + "/settings.ini");
            server   = ini.IniReadValue("MYSQL", "Server");
            database = ini.IniReadValue("MYSQL", "Database");
            uid      = ini.IniReadValue("MYSQL", "Uid");
            password = ini.IniReadValue("MYSQL", "Password");
            string connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" + database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";
            connection = new MySqlConnection(connectionString);
        }

        private bool OpenConnection()
        {
            try
            {
                connection.Open();
                return true;
            }
            catch (MySqlException ex)
            {
                switch (ex.Number)
                {
                    case 0:
                        Console.WriteLine("Cannot connect to server.  Contact administrator");
                        break;

                    case 1045:
                        Console.WriteLine("Invalid username/password, please try again");
                        break;
                }
                return false;
            }
        }

        private bool CloseConnection()
        {
            try
            {
                connection.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public void Update(string id, string param)
        {
            string query = "UPDATE " + ini.IniReadValue("SERVER", "Table") + " SET status='" + param + "' WHERE uid='" + id + "'";

            if (OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandText = query;
                cmd.Connection = connection;
                cmd.ExecuteNonQuery();
                CloseConnection();
            }
        }

    }
}
