﻿using System;
using System.Diagnostics;
using System.IO;

namespace Hurtworld
{
    class Program
    {

        private static DBConnect dbConnect;

        static void Main(string[] args)
        {

            dbConnect = new DBConnect();

            if (args.Length != 0)
            {

                string id    = args[0];
                string type  = args[1];
                string uid   = args[2];
                string verif = args[3];

                if (verif != "85746") {
                    Environment.Exit(0);
                }

                switch (type)
                {
                    case "create":
                        Console.Title = "GameBoxe (Hurtworld) - Création serveur " + uid + " en cours...";
                        dbConnect.Update(uid, "attente");
                        Directory.CreateDirectory(Environment.CurrentDirectory + "/Hurtworld/" + uid);
                        ProcessStartInfo startInfo1 = new ProcessStartInfo();
                        startInfo1.CreateNoWindow   = false;
                        startInfo1.UseShellExecute  = false;
                        startInfo1.FileName         = Environment.CurrentDirectory + "/SteamCMD/steamcmd.exe";
                        startInfo1.WindowStyle      = ProcessWindowStyle.Hidden;
                        startInfo1.Arguments        = "+force_install_dir " + Environment.CurrentDirectory + "/Hurtworld/" + uid + " +login anonymous +app_update 405100 validate +quit";
                        try {
                            using (Process exeProcess = Process.Start(startInfo1)) {
                                exeProcess.WaitForExit();
                            }
                        } catch {
                            dbConnect.Update(uid, "error");
                            Environment.Exit(0);
                        } finally {
                            dbConnect.Update(uid, "success");
                            Environment.Exit(0);
                        }
                        break;
                    case "start":
                        Console.Title = "GameBoxe (Hurtworld) - Start serveur " + uid + " en cours...";
                        break;
                    case "stop":
                        Console.Title = "GameBoxe (Hurtworld) - Stop serveur " + uid + " en cours...";
                        break;
                    case "restart":
                        Console.Title = "GameBoxe (Hurtworld) - Restart serveur " + uid + " en cours...";
                        break;
                    case "update":
                        Console.Title = "GameBoxe (Hurtworld) - Update serveur " + uid + " en cours...";
                        dbConnect.Update(uid, "attente");
                        Directory.CreateDirectory(Environment.CurrentDirectory + "/Hurtworld/" + uid);
                        ProcessStartInfo startInfo2 = new ProcessStartInfo();
                        startInfo2.CreateNoWindow   = false;
                        startInfo2.UseShellExecute  = false;
                        startInfo2.FileName         = Environment.CurrentDirectory + "/SteamCMD/steamcmd.exe";
                        startInfo2.WindowStyle      = ProcessWindowStyle.Hidden;
                        startInfo2.Arguments        = "+force_install_dir " + Environment.CurrentDirectory + "/Hurtworld/" + uid + " +login anonymous +app_update 405100 validate +quit";
                        try
                        {
                            using (Process exeProcess = Process.Start(startInfo2))
                            {
                                exeProcess.WaitForExit();
                            }
                        }
                        catch
                        {
                            dbConnect.Update(uid, "error");
                            Environment.Exit(0);
                        }
                        finally
                        {
                            dbConnect.Update(uid, "success");
                            Environment.Exit(0);
                        }
                        break;
                    case "delete":
                        Console.Title = "GameBoxe (Hurtworld) - Delete serveur " + uid + " en cours...";
                        try {
                            Directory.Delete(Environment.CurrentDirectory + "/Hurtworld/" + uid, true);
                        } catch {
                            dbConnect.Update(uid, "error");
                            Environment.Exit(0);
                        } finally {
                            dbConnect.Update(uid, "success");
                            Environment.Exit(0);
                        }
                        break;
                }

            } else {
                Environment.Exit(0);
            }

            while (true) {}
        }
    }
}